// Errortemplate provides an error tamplate.

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package errortemplate

import (
	"fmt"
)

// Template provides a template error.
type Template string

// templater is the interface satisfied by an object with a Template method.
type templater interface {
	Template() Template // Template returns the associated template
}

// template implements an error.
type template struct {
	p   Template // The original template
	msg string   // The error message
}

// templateWithCause implements an error with cause.
type templateWithCause struct {
	p     Template // The original template
	msg   string   // The error message
	cause error    // The cause
}

/////////////////////////////////////////////////////////////////////////
// template functions
/////////////////////////////////////////////////////////////////////////

// Error returns the associated error message.
func (e *template) Error() string {
	return e.msg
}

// Template returns the template used to create e.
func (e *template) Template() Template {
	return e.p
}

// Is returns true iff the target error was created from the same template as e.
func (e *template) Is(target error) bool {
	return e.p.Is(target)
}

/////////////////////////////////////////////////////////////////////////
// templateWithCause functions
/////////////////////////////////////////////////////////////////////////

// Error returns the associated error message.
func (e *templateWithCause) Error() string {
	return e.msg + ": " + e.cause.Error()
}

// Template returns the template used to create e.
func (e *templateWithCause) Template() Template {
	return e.p
}

// Is returns true iff the target error was created from the same template as e.
func (e *templateWithCause) Is(target error) bool {
	return e.p.Is(target)
}

// Unwrap returns the wrapped error.
func (e *templateWithCause) Unwrap() error {
	return e.cause
}

// Cause is an alias for Unwrap, added to satisfy the causer interface in package "pkg/errors".
func (e *templateWithCause) Cause() error {
	return e.cause
}

/////////////////////////////////////////////////////////////////////////
// Template functions
/////////////////////////////////////////////////////////////////////////

// New returns an error from the template.
func (p Template) New(args ...interface{}) error {
	return &template{
		p:   p,
		msg: fmt.Sprintf(p.String(), args...),
	}
}

// Wrap returns an error from the template, wrapping err.
func (p Template) Wrap(err error, args ...interface{}) error {
	if err == nil {
		return p.New(args...)
	}
	return &templateWithCause{
		p:     p,
		msg:   fmt.Sprintf(p.String(), args...),
		cause: err,
	}
}

// String returns the text defining this template.
func (p Template) String() string {
	return string(p)
}

// Is returns true iff the given error e was created from the template p.
func (p Template) Is(e error) bool {
	te, ok := e.(templater)
	return ok && te.Template() == p
}
